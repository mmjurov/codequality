FROM composer:2 as composer
FROM php:7.4-fpm-alpine
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

# Библиотеки php, нужные для работы проекта
RUN install-php-extensions zip
COPY . .
RUN COMPOSER_ALLOW_SUPERUSER=1 composer install --no-interaction --no-progress --optimize-autoloader